use crate::digit::{Digit, RADIX};
use anyhow::{anyhow, Error, Result};
use std::cmp;
use std::convert::TryFrom;
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Eq, Ord, Clone, Debug)]
pub struct BigUInt {
    digits: Vec<Digit>,
}

impl BigUInt {
    pub fn len(&self) -> usize {
        self.digits.len()
    }
    pub fn is_empty(&self) -> bool {
        self.num_digits() == 0
    }
    pub fn num_digits(&self) -> usize {
        self.digits.len() - self.digits.iter().rev().take_while(|x| x == &&0).count()
    }

    fn solve_overflow(&mut self, mut overflow: Digit, mut index: usize) {
        while overflow > 0 {
            if index >= self.len() {
                self.digits.resize(index + 2, 0);
            }

            if let None = self.digits[index].checked_add(overflow) {
                println!("Overflow");
            }
            let sum = self.digits[index] + overflow;
            if sum > RADIX {
                self.digits[index] = sum % RADIX;
                overflow = 1;
            } else {
                self.digits[index] = sum;
                overflow = 0;
            }
            index += 1;
        }
    }

    fn solve_negative_overflow(&mut self, mut overflow: Digit, mut index: usize) {
        while overflow > 0 {
            if index >= self.len() {
                self.digits.resize(index + 2, 0);
            }

            if let Some(sub) = self.digits[index].checked_sub(overflow) {
                self.digits[index] = sub;
                overflow = 0;
            } else {
                self.digits[index] = RADIX - (overflow - self.digits[index]);
                overflow = 1;
            }

            index += 1;
        }
    }
}

impl SubAssign for BigUInt {
    fn sub_assign(&mut self, rhs: Self) {
        if *self < rhs {
            panic!("Attempt to subtract with overflow");
        }
        rhs.digits
            .iter()
            .enumerate()
            .for_each(|(i, d)| self.solve_negative_overflow(*d, i));
    }
}

impl Sub for BigUInt {
    type Output = Self;
    fn sub(mut self, rhs: Self) -> Self::Output {
        self -= rhs;
        self
    }
}

impl AddAssign for BigUInt {
    fn add_assign(&mut self, rhs: Self) {
        rhs.digits
            .iter()
            .enumerate()
            .for_each(|(i, d)| self.solve_overflow(*d, i));
    }
}

impl Add for BigUInt {
    type Output = BigUInt;
    fn add(mut self, rhs: Self) -> Self::Output {
        self += rhs;
        self
    }
}

impl From<usize> for BigUInt {
    fn from(value: usize) -> Self {
        BigUInt {
            digits: vec![value as Digit],
        }
    }
}

impl TryFrom<&str> for BigUInt {
    type Error = Error;
    fn try_from(value: &str) -> Result<Self> {
        let digits: Vec<u128> = value
            .chars()
            .rev()
            .collect::<Vec<char>>()
            .chunks((RADIX as f64).log10() as usize)
            .map(|x| {
                x.iter()
                    .rev()
                    .collect::<String>()
                    .parse::<Digit>()
                    .or_else(|_| Err(anyhow!("Couldn't parse number")))
            })
            .collect::<Result<Vec<Digit>>>()?;

        Ok(BigUInt { digits: { digits } })
    }
}

impl PartialEq for BigUInt {
    fn eq(&self, other: &Self) -> bool {
        self.partial_cmp(other) == Some(cmp::Ordering::Equal)
    }
}

impl PartialOrd for BigUInt {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        use cmp::Ordering;
        match self.num_digits().cmp(&other.num_digits()) {
            Ordering::Greater => Some(Ordering::Greater),
            Ordering::Less => Some(Ordering::Less),
            Ordering::Equal => {
                for i in (0..self.num_digits()).rev() {
                    match self.digits[i].cmp(&other.digits[i]) {
                        Ordering::Less => return Some(Ordering::Less),
                        Ordering::Greater => return Some(Ordering::Greater),
                        Ordering::Equal => {}
                    }
                }
                Some(Ordering::Equal)
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn lol() {
        let a = BigUInt::try_from("340282366920938463463374607431768211455").unwrap();
        let b = BigUInt::try_from("340282366920938463463374607431768211455").unwrap();

        assert_eq!(
            a + b,
            BigUInt::try_from("680564733841876926926749214863536422910").unwrap()
        );
    }

    #[test]
    fn biguint_add() {
        let mut a = BigUInt::try_from("123456789123456789123456798123456789").unwrap();
        let b = BigUInt::try_from("987654321987654321987654321987654321987654321").unwrap();

        assert_eq!(
            (a.clone() + b.clone()),
            BigUInt::try_from("987654322111111111111111111111111120111111110").unwrap()
        );

        a += b;
        assert_eq!(
            a,
            BigUInt::try_from("987654322111111111111111111111111120111111110").unwrap()
        )
    }

    #[test]
    fn biguint_sub() {
        let a = BigUInt::try_from("99999999999999999999").unwrap();
        let mut b = BigUInt::try_from("12345678912345687911111111111111111").unwrap();

        assert_eq!(
            (b.clone() - a.clone()),
            BigUInt::try_from("12345678912345587911111111111111112").unwrap()
        );

        b -= a;
        assert_eq!(
            b,
            BigUInt::try_from("12345678912345587911111111111111112").unwrap()
        )
    }

    #[test]
    #[should_panic(expected = "Attempt to subtract with overflow")]
    fn biguint_sub_overflow() {
        let mut a = BigUInt::try_from("123456789123456789123456798123456789").unwrap();
        let b = BigUInt::try_from("987654321987654321987654321987654321987654321").unwrap();
        a -= b;
    }

    #[test]
    fn biguint_from_usize() {
        assert_eq!(BigUInt::from(987654321).digits[0], 987654321);
    }

    #[test]
    fn biguint_cmp() {
        let a = BigUInt::try_from("123456789123456789123456798123456789").unwrap();
        let b = BigUInt::try_from("987654321987654321987654321987654321987654321").unwrap();
        println!("{:?}\n{:?}", a, b);

        assert!(a == a);
        assert!(a < b);
        assert!(b > a);
        assert!(b > a.clone() + a);
    }
}
