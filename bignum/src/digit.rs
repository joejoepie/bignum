pub(crate) type Digit = u128;
pub(crate) const RADIX: Digit = std::u128::MAX;
